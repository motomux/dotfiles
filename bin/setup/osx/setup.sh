#!/bin/bash

# Setup.sh for Mac OSX

# Install Homebrew
# Requires command lines tools for Xcode
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install with Homebrew
brew update
brew upgrade

brew tap homebrew/versions
brew tap homebrew/binary

brew install nodebrew
brew install fzf
brew install tmux
brew install zsh
brew install lua
brew install vim --with-cscope --with-lua
brew install git
brew install tig
brew install the_silver_searcher
brew install coreutils
brew install gnu-sed --with-default-names
brew install markdown
brew install wget
brew install teensy_loader_cli
brew install tree

brew tap homebrew/dupes
brew install rsync

# Install Homebrew cask
brew tap caskroom/homebrew-cask
brew install brew-cask
brew cask install google-chrome
brew cask install google-japanese-ime
brew cask install sequel-pro
brew cask install android-file-transfer
brew cask install dropbox
brew cask install karabiner

sudo easy_install pip
pip install awscli

# Install Golang
brew install golang

# go
go get golang.org/x/tools/cmd/...
go get github.com/nsf/gocode
go get github.com/motemen/gore
go get golang.org/x/tools/cmd/godoc
go get github.com/k0kubun/pp
go get github.com/golang/lint/golint
go get -u github.com/alecthomas/gometalinter
gometalinter --install
