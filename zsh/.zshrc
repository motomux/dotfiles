### .zshrc ###

# cd
setopt auto_cd
setopt auto_pushd
setopt correct

# Keybind
bindkey -e

# History
HISTFILE=~/.zsh_history
HISTSIZE=50000
SAVEHIST=50000
setopt hist_ignore_dups
setopt share_history
setopt hist_reduce_blanks
setopt hist_ignore_all_dups
setopt hist_ignore_space

# Keybind for history
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^p" history-beginning-search-backward-end
bindkey "^n" history-beginning-search-forward-end
bindkey "\\ep" history-beginning-search-backward-end
bindkey "\\en" history-beginning-search-forward-end

# Completion
autoload -U compinit
compinit

# Alias
alias g='git'
alias l='ls -AFlG'
alias jxa='osascript -l JavaScript'
alias v='vim'
alias f='fzf'
alias k='kubectl'

# Alias for git
alias gco='git checkout `git branch | fzf`'
alias gbd='git branch -D `git branch | fzf`'
alias gba='git branch -a | fzf'

# Alias for dotfiles
alias zshrc='v ~/.zshrc; source ~/.zshrc'
alias vimrc='v ~/.vimrc'
alias gitconfig='v ~/.gitconfig'
alias gitignore='v ~/.gitignore'
alias tmux.conf='v ~/.tmux.conf'
alias tigrc='v ~/.tigrc'

# Color
autoload colors
colors

# Prompt
PROMPT='[%F{33}%n%f@%F{004}%m%f]%(?,%F{064},%F{001})%#%f '

# Word separator
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

# RPrompt
autoload -Uz vcs_info
zstyle 'vcs_info:*' enable git
zstyle ':vcs_info:*' max-exports 3
zstyle ':vcs_info:*' formats '%r' '%S' '%b'
zstyle ':vcs_info:*' actionformats '%r' '%S' '%b|%a'

function print_rprompt () {
  STY= LANG=en_US.UTF-8 vcs_info
  if [[ -n "$vcs_info_msg_0_" ]]; then
    print -n "(%F{33}$vcs_info_msg_2_%f)" # blue
    print -n "[%F{136}$vcs_info_msg_0_%f/%F{166}%30<...<$vcs_info_msg_1_%<<%f]" # yellow orange
  else
    print -nD "[%F{136}%60<..<%~%<<%f]" # yellow
  fi
}

setopt prompt_subst
RPROMPT='`print_rprompt`'

# fzf
function fzf-select-history() {
  BUFFER=$(history -n -r 1 | \
      fzf --query "$LBUFFER")
  CURSOR=$#BUFFER
  zle clear-screen
}
zle -N fzf-select-history
bindkey '^r' fzf-select-history

# Import local zshrc
if [[ -e ~/dotfiles.local/zsh/.import_zshrc ]]; then
  source ~/dotfiles.local/zsh/.import_zshrc
fi

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

# AWS-CLI
function aws-switch-profile() {
  cmd="export AWS_DEFAULT_PROFILE=${1}"
  echo ${cmd}
  eval ${cmd}
}
# Docker
function docker-ps-cid() {
  docker ps -a | fzf | awk '{print $1}'
}

# Golang
export GOROOT=/usr/local/go
export GOPATH=${HOME}/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOBIN

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
